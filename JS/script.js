"use strict";

// TABS

let tabs = document.querySelector(".services-tabs");
let tabsInfo = document.querySelectorAll(".services-info");

tabs.addEventListener("click", function (e) {
  let position;
  let elements = tabs.children;
  for (let i = 0; i < elements.length; i++) {
    elements[i].classList.remove("services--focus");
    if (elements[i] === e.target) {
      position = i;
      elements[i].classList.add("services--focus");
    }
  }

  tabsInfo.forEach((element, index) => {
    element.classList.add("hidden");
    if (index === position) {
      element.classList.toggle("hidden");
    }
  });
});

let workExample = document.querySelectorAll(".work-example");
let workFilter = document.querySelector(".work-filter");

workFilter.addEventListener("click", (e) => {
  for (let elem of workFilter.children) {
    elem.classList.remove("work-focus");
    if (elem == e.target) {
      elem.classList.add("work-focus");
    }
  }
  workExample.forEach((element) => {
    if (e.target.dataset.type === "all") {
      element.classList.remove("hidden");
    } else {
      element.classList.add("hidden");
      if (e.target.dataset.type === element.dataset.type) {
        element.classList.remove("hidden");
      }
    }
  });
});

// ADD IMG WORK

const containerWork = document.querySelector(".work-example-place");
const workBtn = document.querySelector("#work-btn");
const loaderWork = document.querySelector(".loader-work");
const numberImg = 12;
let workFlag = false;

const workAllSrc = {
  graphicDesignSrc: [
    "./images/graphic-design/graphic-design4.jpg",
    "./images/graphic-design/graphic-design5.jpg",
    "./images/graphic-design/graphic-design6.jpg",
  ],
  webDesignSrc: [
    "./images/web-design/web-design5.jpg",
    "./images/web-design/web-design6.jpg",
    "./images/web-design/web-design7.jpg",
  ],
  landingPagesSrc: [
    "./images/landing-page/landing-page3.jpg",
    "./images/landing-page/landing-page4.jpg",
    "./images/landing-page/landing-page5.jpg",
  ],
  wordpressSrc: [
    "./images/wordpress/wordpress4.jpg",
    "./images/wordpress/wordpress5.jpg",
    "./images/wordpress/wordpress6.jpg",
  ],
};
function loadImg(number) {
  let result = [];

  for (let i = 0; i < number; i++) {
    if (i < 3) {
      const graphicDesign = document
        .querySelector("#graphic-design")
        .cloneNode(true);
      graphicDesign.firstElementChild.src = workAllSrc.graphicDesignSrc[i % 3];
      result.push(graphicDesign);
    }
    if (i >= 3 && i < 6) {
      const webDesign = document.querySelector("#web-design").cloneNode(true);
      webDesign.firstElementChild.src = workAllSrc.webDesignSrc[i % 3];
      result.push(webDesign);
    }
    if (i >= 6 && i < 9) {
      const landingPages = document
        .querySelector("#landing-pages")
        .cloneNode(true);
      landingPages.firstElementChild.src = workAllSrc.landingPagesSrc[i % 3];
      result.push(landingPages);
    }
    if (i >= 9) {
      const wordpress = document.querySelector("#wordpress").cloneNode(true);
      wordpress.firstElementChild.src = workAllSrc.wordpressSrc[i % 3];
      result.push(wordpress);
    }
  }
  shuffleArray(result);
  return result;
}

workBtn.addEventListener("click", (e) => {
  e.preventDefault();

  loaderWork.classList.toggle("hidden");
  workBtn.classList.toggle("hidden");

  setTimeout(() => {
    loaderWork.classList.toggle("hidden");
    if (workFlag) {
      containerWork.append(...loadImg(numberImg));
      workBtn.remove();
      workFlag = false;
    } else {
      containerWork.append(...loadImg(numberImg));
      workBtn.classList.toggle("hidden");
      workFlag = true;
    }
    workExample = document.querySelectorAll(".work-example");
  }, 2000);
});

// MASONRY

let $grid = $(".grid").masonry({
  itemSelector: ".item",
  columnWidth: ".grid-sizer",
  horisontalOrder: true,
  gutter: 20,
  filWidth: true,
});

window.addEventListener("load", () => {
  $grid.masonry();
});

// MASONRY ADD IMG

let masonryFlag = false;
const containerGallery = document.querySelector(".gallery-container");
const galleryBtn = document.querySelector("#btn--bestimage");
const loaderGallery = document.querySelector(".loader-gallery");
const galleryAllSrc = [
  "./images/best-images/bes-images1.jpg",
  "./images/best-images/bes-images2.jpg",
  "./images/best-images/bes-images3.jpg",
  "./images/best-images/bes-images3-1.jpg",
  "./images/best-images/bes-images3-2.jpg",
  "./images/best-images/bes-images3-3.jpg",
  "./images/best-images/bes-images3-4.jpg",
  "./images/best-images/bes-images3-5.jpg",
  "./images/best-images/bes-images4.jpg",
  "./images/best-images/bes-images5.jpg",
  "./images/best-images/bes-images6.jpg",
  "./images/best-images/bes-images7.jpg",
];

function addGalleryImg(number) {
  const result = [];

  for (let i = 0; i < number; i++) {
    const galleryImgPlace = document.querySelector(".item").cloneNode(true);
    galleryImgPlace.firstElementChild.src = galleryAllSrc[i];
    result.push(galleryImgPlace);
  }
  shuffleArray(result);
  return result;
}

galleryBtn.addEventListener("click", (e) => {
  e.preventDefault();

  loaderGallery.classList.toggle("hidden");
  galleryBtn.classList.toggle("hidden");

  setTimeout(() => {
    let $elem = $(addGalleryImg(numberImg));
    loaderGallery.classList.toggle("hidden");
    if (masonryFlag) {
      galleryBtn.remove();
    } else {
      galleryBtn.classList.toggle("hidden");
      masonryFlag = true;
    }
    $grid.append($elem).masonry("appended", $elem);
  }, 2000);
});

// SWIPER

let mySwiper = new Swiper(".swiper-container", {
  direction: "horizontal",

  pagination: {
    el: ".swiper-pagination",
    clickable: true,
    renderBullet: function (index, className) {
      let src = this.slides[index]
        .querySelector(".about-ham-img")
        .getAttribute("src");
      return `<img class="${className}" src="${src}" alt="Photo">`;
    },
  },

  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}
